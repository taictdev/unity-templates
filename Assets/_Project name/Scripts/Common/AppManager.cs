using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using DjVoteGame.Utils;
using UnityEngine.AddressableAssets;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using Skywatch.AssetManagement;
using DjVoteGame.Api;
using DjVoteGame.Config;
using DjVoteGame.UI;

namespace DjVoteGame.Common
{
    public class AppManager : AutoSingletonMono<AppManager>
    {
        private Action _onSceneLoaded = null;

        #region PUBLIC_FIELDS

        public Dictionary<ItemType, InventoryItemConfig> ItemConfigs = new Dictionary<ItemType, InventoryItemConfig>();
        public SceneName BeforeScene = SceneName.None;

        #endregion PUBLIC_FIELDS

        #region PRIVATE_METHODS

        protected override void Init()
        {
            base.Init();
            Application.targetFrameRate = 120;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            AudioSource[] audioSources = GameObject.FindObjectsOfType<AudioSource>();
            foreach (AudioSource audioSource in audioSources)
            {
                if (audioSource.outputAudioMixerGroup != null)
                {
                    audioSource.outputAudioMixerGroup = AudioPlayer.Instance.GetAudioMixerGroup(audioSource.outputAudioMixerGroup.name);
                }
            }

            _onSceneLoaded?.Invoke();
        }

        private void OnSceneUnLoaded(Scene scene)
        {
        }

        #endregion PRIVATE_METHODS

        public override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnLoaded;
            Application.runInBackground = true;
        }

        public async void ChangeScene(SceneName sceneName, bool fadeEffect = true, Action onSceneLoaded = null)
        {
            if (string.IsNullOrEmpty(sceneName.ToString()))
                return;
            UIFrameManager.Instance?.UIFrame?.ShowPanel(ScreenIds.UILoadingPanel, new LoadingProperties() { HasBackGround = true });
            Debug.Log($"ChangeScene : {sceneName}");
            // Load data befor goto Scene
            //...
            // Load data done
            await Addressables.LoadSceneAsync(sceneName.ToString());
        }

        public AsyncOperationHandle InitItemConfigs()
        {
            ItemConfigs.Clear();
            AsyncOperationHandle loader = AssetManager.LoadAssetsByLabelAsync("itemconfigs");
            loader.Completed += op =>
            {
                List<object> results = op.Result as List<object>;
                foreach (object obj in results)
                {
                    InventoryItemConfig itemConfig = obj as InventoryItemConfig;
                    if (itemConfig == null)
                        continue;

                    ItemConfigs.Add(itemConfig.ItemType, itemConfig);
                }
            };
            return loader;
        }
    }
}
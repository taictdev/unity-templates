using System.Collections;
using System.Collections.Generic;
using DjVoteGame.Common;
using Duck.Http.Service;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace DjVoteGame.Api.Account
{
    public class VerifyRegisterData
    {
        public string token;
        public string timestamp;
        public string refresh_token;
    }

    public class VerifyRegisterAPI : HttpApi<BasicData<VerifyRegisterData>>
    {
        public string Email;
        public string VerifyCode;
        public string UserID;
        protected override string ApiUrl => "/api/v1/user/{id}/verify";

        protected override IHttpRequest GetHttpRequest()
        {
            JObject param = new JObject();
            param.Add("email", Email);
            param.Add("verify_code", VerifyCode);
            return NetworkManager.Instance.HttpPostNoAuthen(NetworkManager.Instance.apiServer, string.Format(ApiUrl, UserID), param);
        }
    }
}

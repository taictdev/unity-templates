﻿using UnityEngine;
using System.Collections;
using Duck.Http.Service;
using Newtonsoft.Json.Linq;
using DjVoteGame.Common;
using System.Collections.Generic;
using System;

namespace DjVoteGame.Api.Account
{
    [Serializable]
    public class RegisterAccountResponseData
    {
        public List<string> data;
    }

    public class RegisterAccountAPI : HttpApi<BasicData<RegisterAccountResponseData>>
    {
        public string Firstname;
        public string Lastname;
        public string Email;
        public string Username;
        public string Password;
        public string Confirm_password;

        protected override string ApiUrl => "/api/v1/user";

        protected override IHttpRequest GetHttpRequest()
        {
            JObject param = new JObject();
            param.Add("firstname", Firstname);
            param.Add("lastname", Lastname);
            param.Add("email", Email);
            param.Add("username", Username);
            param.Add("password", Password);
            param.Add("confirm_password", Confirm_password);
            return NetworkManager.Instance.HttpPostNoAuthen(NetworkManager.Instance.apiServer, ApiUrl, param);
        }
    }
}
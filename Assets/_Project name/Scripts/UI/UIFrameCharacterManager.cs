using System.Collections;
using System.Collections.Generic;
using DjVoteGame.Utils;
using System;
using deVoid.UIFramework;
using UnityEngine;
using DjVoteGame;
using UnityEngine.Rendering.Universal;

public class UIFrameCharacterManager : MonoBehaviour
{
    [SerializeField] private List<UISettings> uISettings;
    public UIFrame uIFrame { get; private set; }

    // Start is called before the first frame update
    private void Awake()
    {
        foreach (UISettings uiSetting in uISettings)
            uIFrame = uiSetting.CreateUIInstance(true, uIFrame);
        var cameraData = Camera.main.GetUniversalAdditionalCameraData();
        cameraData.cameraStack.Add(uIFrame.GetComponentInChildren<Camera>());
        uIFrame.transform.SetParent(transform);
    }
}

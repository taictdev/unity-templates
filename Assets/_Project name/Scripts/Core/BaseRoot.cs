using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DjVoteGame.Core
{
    public class BaseRoot : MonoBehaviour
    {

        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
        }

        protected virtual void Update()
        {
        }
    }
}

using UnityEngine;
using DG.Tweening;

namespace DjVoteGame.Utils
{
    public delegate void OnFadeComplete();

    public class FadeAnimation : AutoSingletonMono<FadeAnimation>
    {
        public GameObject icon;

        public override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }

        private void FixedUpdate()
        {
            icon.transform.Rotate(Vector3.forward, -100 * Time.deltaTime);
            foreach (Transform c in icon.transform)
                c.Rotate(Vector3.forward, 100 * Time.deltaTime);
        }

        public void FadeIn(OnFadeComplete onComplete, bool showIcon = false)
        {
            icon.gameObject.SetActive(showIcon);
            
            CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.blocksRaycasts = true;
            GetComponent<CanvasGroup>().DOFade(1.0f, 1.0f).OnComplete(() =>
            {
                if (onComplete != null)
                    onComplete.Invoke();
            }).SetId(this);
        }

        public void FadeOut(OnFadeComplete onComplete, bool showIcon = false)
        {
            icon.gameObject.SetActive(showIcon);
            
            CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.DOFade(0.0f, 1.0f).OnComplete(() =>
            {
                canvasGroup.blocksRaycasts = false;
                if (onComplete != null)
                    onComplete.Invoke();
            }).SetId(this);
        }

        public void ShowLoading(bool show)
		{
			icon.gameObject.SetActive(show);
            CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.blocksRaycasts = show;
			canvasGroup.DOFade(show ? 0.5f : 0f, 0.5f).SetId(this);
		}

        public void Stop()
        {
            DOTween.Kill(this);
            FadeOut(null, true);
        }
    }
}

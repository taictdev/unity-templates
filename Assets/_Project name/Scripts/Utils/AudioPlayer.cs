using UnityEngine;
using UnityEngine.Audio;

namespace DjVoteGame.Utils
{
    public class AudioInfo
    {
        public AudioSource audio;
        public float lastPlayingTime;
    }

    public class AudioPlayer : AutoSingletonMono<AudioPlayer>
    {
        private static string sfxBattleVol = "sfxBattleVol";
        private static string sfxVol = "sfxVol";
        private static string musicVol = "musicVol";
        private static string masterVol = "masterVol";

        [SerializeField] private AudioMixer audioMixer;

        public AudioMixerGroup GetAudioMixerGroup(string group)
        {
            if (audioMixer == null)
                return null;

            AudioMixerGroup[] groups = audioMixer.FindMatchingGroups(group);
            if (groups == null || groups.Length <= 0)
                return null;

            return groups[0];
        }

        public override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            audioMixer.SetFloat(sfxVol, PlayerPrefs.GetFloat(sfxVol, 0.0f));
            audioMixer.SetFloat(musicVol, PlayerPrefs.GetFloat(musicVol, 0.0f));
            audioMixer.SetFloat(masterVol, PlayerPrefs.GetFloat(masterVol, 0.0f));
        }

        public void SetBattleVolume(float vol)
        {
            vol = Mathf.Clamp01(vol) * 80 - 80;
            audioMixer.SetFloat(sfxBattleVol, vol);
        }

        public void SetSoundVolume(float vol)
        {
            vol = Mathf.Clamp01(vol) * 80 - 80;
            audioMixer.SetFloat(sfxVol, vol);
            PlayerPrefs.SetFloat(sfxVol, vol);
        }

        public float GetSoundVolume()
        {
            audioMixer.GetFloat(sfxVol, out float vol);
            return (vol + 80) / 80;
        }

        public void SetMusicVolume(float vol)
        {
            vol = Mathf.Clamp01(vol) * 80 - 80;
            audioMixer.SetFloat(musicVol, vol);
            PlayerPrefs.SetFloat(musicVol, vol);
        }

        public float GetMusicVolume()
        {
            audioMixer.GetFloat(musicVol, out float vol);
            return (vol + 80) / 80;
        }

        public void SetMasterVolume(float vol)
        {
            vol = Mathf.Clamp01(vol) * 100 - 80;
            audioMixer.SetFloat(masterVol, vol);
            PlayerPrefs.SetFloat(masterVol, vol);
        }

        public float GetMasterVolume()
        {
            audioMixer.GetFloat(masterVol, out float vol);
            return (vol + 80) / 100;
        }

        public void PlayAudio(AudioSource audio, int parts = 1)
        {
            if (audio != null)
            {
                if (audio.outputAudioMixerGroup != null)
                {
                    audio.outputAudioMixerGroup = GetAudioMixerGroup(audio.outputAudioMixerGroup.name);
                }

                if (parts > 1)
                {
                    audio.time = audio.clip.length * (float)Random.Range(0, parts) / parts;
                    audio.Play();
                    audio.SetScheduledEndTime(AudioSettings.dspTime + audio.clip.length / parts);
                }
                else
                {
                    audio.Play();
                }
            }
        }

        public void StopAudio(AudioSource audio)
        {
            if (audio != null && audio.isPlaying)
            {
                audio.Stop();
            }
        }
    }
}
﻿using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Devdog.QuestSystemPro.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class TextAnimator : MonoBehaviour, ITextAnimator
    {
        public enum AnimationType
        {
            LetterStep,
            WordStep,
            FadeIn
        }

        public AnimationType animationType;
        public float animationSpeed = 1f;

        private TMP_Text _text;

        private bool _showNow = false;

        protected void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }

        public void AnimateText(string msg)
        {
            _showNow = false;
            _text.text = "";
            switch (animationType)
            {
                case AnimationType.LetterStep:
                    StartCoroutine(_SetTextLetterStep(msg));
                    break;

                case AnimationType.WordStep:
                    StartCoroutine(_SetTextWordStep(msg));
                    break;

                case AnimationType.FadeIn:
                    StartCoroutine(_SetTextFadeIn(msg));
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public UniTask AnimateTextAsync(string msg)
        {
            _showNow = false;
            _text.text = "";

            switch (animationType)
            {
                case AnimationType.LetterStep:
                    return _SetTextLetterStep(msg).ToUniTask();

                case AnimationType.WordStep:
                    return _SetTextWordStep(msg).ToUniTask();

                case AnimationType.FadeIn:
                    return _SetTextFadeIn(msg).ToUniTask();

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerator _SetTextLetterStep(string msg)
        {
            var waitTime = new WaitForSeconds(1f / animationSpeed);
            _text.text = "";

            yield return waitTime;

            int index = 0;
            while (index < msg.Length && !_showNow)
            {
                _text.text += msg[index];
                yield return waitTime;
                index++;
            }

            // showNow trigger turn on
            if (_showNow)
            {
                ShowNow(msg);
            }
        }

        private IEnumerator _SetTextWordStep(string msg)
        {
            var waitTime = new WaitForSeconds(1f / animationSpeed);

            var words = msg.Split(' ');

            int index = 0;
            while (index < words.Length && !_showNow)
            {
                _text.text += words[index] + " ";
                yield return waitTime;
                index++;
            }

            // showNow trigger turn on
            if (_showNow)
            {
                ShowNow(msg);
            }
        }

        private IEnumerator _SetTextFadeIn(string msg)
        {
            _text.text = msg;

            var startColor = _text.color;
            startColor.a = 0f;
            _text.color = startColor;

            float time = 1f / animationSpeed;
            float timer = 0f;
            while (timer < time && !_showNow)
            {
                timer += Time.deltaTime;
                startColor.a += animationSpeed * Time.deltaTime;
                _text.color = startColor;

                yield return null;
            }

            // showNow trigger turn on
            if (_showNow)
            {
                ShowNow(msg);
            }
        }

        public void TurnOnShowNow()
        {
            _showNow = true;
        }

        private void ShowNow(string ms)
        {
            _text.text = ms;
        }
    }
}
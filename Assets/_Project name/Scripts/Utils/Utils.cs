using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace DjVoteGame.Utils
{
    public static class Utility
    {
        public static string Sha256Hash(string value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        public static bool UIHasFocus()
        {
            if (EventSystem.current.currentSelectedGameObject != null && (EventSystem.current.currentSelectedGameObject.GetComponent<InputField>() != null
                || EventSystem.current.currentSelectedGameObject.GetComponent<TMP_InputField>() != null))
                return true;
            return false;
        }

        public static Vector2 ScreenPointCanvasScaler(CanvasScaler scaler, Vector2 pos)
        {
            float scalex = pos.x / Screen.width;
            float scaley = pos.y / Screen.height;
            return new Vector2(scaler.referenceResolution.x * scalex, scaler.referenceResolution.y * scaley);
        }

        public static List<List<T>> Partition<T>(List<T> coll, int n)
        {
            var count = coll.Count;
            var ret = new List<List<T>>(count / n);
            List<T> curList = null;

            for (var i = 0; i < count; i++)
            {
                if (curList == null || curList.Count == n)
                {
                    var size = Math.Min(n, count - i);
                    curList = new List<T>(size);
                    ret.Add(curList);
                }

                curList.Add(coll[i]);
            }

            return ret;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool LayerMaskContains(LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }

        public static void BFSTransform(Transform root, Action<Transform> process)
        {
            var q = new Queue<Transform>();
            var visited = new HashSet<int>();
            q.Enqueue(root);

            while (q.Count > 0)
            {
                var cur = q.Dequeue();
                var id = cur.GetInstanceID();
                if (!visited.Contains(id))
                {
                    process(cur);
                    foreach (Transform child in cur.transform)
                    {
                        q.Enqueue(child);
                    }
                    visited.Add(id);
                }
            }
        }

        /// <summary>
        /// Get Color from Hex string FF00FFAA
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Color GetColorFromString(string color)
        {
            float red = Hex_to_Dec01(color.Substring(0, 2));
            float green = Hex_to_Dec01(color.Substring(2, 2));
            float blue = Hex_to_Dec01(color.Substring(4, 2));
            float alpha = 1f;
            if (color.Length >= 8)
            {
                // Color string contains alpha
                alpha = Hex_to_Dec01(color.Substring(6, 2));
            }
            return new Color(red, green, blue, alpha);
        }

        /// <summary>
        /// Returns a float between 0->1
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static float Hex_to_Dec01(string hex)
        {
            return Hex_to_Dec(hex) / 255f;
        }

        /// <summary>
        /// Returns 0-255
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static int Hex_to_Dec(string hex)
        {
            return Convert.ToInt32(hex, 16);
        }

        public static string XorString(string key, string input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.Length; i++)
                sb.Append((char)(input[i] ^ key[(i % key.Length)]));
            String result = sb.ToString();

            return result;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static bool ScrambledEquals<T>(this IEnumerable<T> list1, IEnumerable<T> list2)
        {
            var deletedItems = list1.Except(list2).Any();
            var newItems = list2.Except(list1).Any();
            return !newItems && !deletedItems;
        }

        private static bool ArePermutations<T>(IList<T> list1, IList<T> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            var l1 = list1.ToLookup(t => t);
            var l2 = list2.ToLookup(t => t);

            return l1.Count == l2.Count
                && l1.All(group => l2.Contains(group.Key) && l2[group.Key].Count() == group.Count());
        }
    }
}
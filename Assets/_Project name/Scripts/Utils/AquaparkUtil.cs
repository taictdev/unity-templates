using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DjVoteGame.Utils
{
    public class AquaparkUtil
    {
        public static string RankSuffix(int rank)
        {
            string sufix = rank == 1 ? "st" : rank == 2 ? "nd" : rank == 3 ? "rd" : "th";
            return rank + sufix;
        }
    }
}